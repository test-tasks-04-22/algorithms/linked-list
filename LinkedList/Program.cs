﻿var linkedList = new LinkedList.LinkedList<string>();
linkedList.Add("1");
linkedList.Add("2");
linkedList.Add("3");
linkedList.Add("4");

foreach(var item in linkedList)
{
    Console.WriteLine($"{item} добавлено в список");
}

linkedList.Remove("1");
Console.Write("Список после удаления элемента: ");
foreach (var item in linkedList)
{
    Console.Write($"{item} ");
}

var isPresent = linkedList.Contains("3");
Console.Write("\n");
Console.WriteLine(isPresent == true ? "3 присутствует" : "3 отсутствует");

linkedList.AppendFirst("10");
Console.Write("Список после добавления нового элемента: ");
foreach (var item in linkedList)
{
    Console.Write($"{item} ");
}

﻿using System.Diagnostics;

namespace LinkedList;
using System.Collections;
using System.Collections.Generic;

public class Node<T>
{
    public Node(T data)
    {
        Data = data;
    }
    public T Data { get; set; }
    public Node<T>? Next { get; set; }
}

public class LinkedList<T> : IEnumerable<T>
{
    private Node<T>? _head;
    private Node<T>? _tail;

    public void Add(T data)
    {
        var node = new Node<T>(data);

        if (_head == null)
            _head = node;
        else
        {
            Debug.Assert(_tail != null, nameof(_tail) + " != null");
            _tail.Next = node;
        }

        _tail = node;
        Count++;
    }

    public bool Remove(T data)
    {
        var current = _head;
        Node<T>? previous = null;

        while (current != null)
        {
            Debug.Assert(current.Data != null, "current.Data != null");
            if (current.Data.Equals(data))
            {
                if (previous != null)
                {
                    previous.Next = current.Next;
                    if (current.Next == null)
                        _tail = previous;
                }
                else
                {
                    _head = _head?.Next;
                    if (_head == null)
                        _tail = null;
                }
                Count--;
                return true;
            }

            previous = current;
            current = current.Next;
        }
        return false;
    }

    private int Count { get; set; }
    public bool IsEmpty => Count == 0;

    public void Clear()
    {
        _head = null;
        _tail = null;
        Count = 0;
    }

    public bool Contains(T data)
    {
        var current = _head;
        while (current != null)
        {
            Debug.Assert(current.Data != null, "current.Data != null");
            if (current.Data.Equals(data))
                return true;
            current = current.Next;
        }
        return false;
    }

    public void AppendFirst(T data)
    {
        var node = new Node<T>(data)
        {
            Next = _head
        };
        _head = node;
        if (Count == 0)
            _tail = _head;
        Count++;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable)this).GetEnumerator();
    }

    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
        var current = _head;
        while (current != null)
        {
            yield return current.Data;
            current = current.Next;
        }
    }
}

